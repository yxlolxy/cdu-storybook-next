import{n as s,t as c}from"./dom-D5Pft_32.js";import{p as d,t as h,c as l,i as a}from"./Events-DxCy3JHV.js";var r=`<svg class="icon" aria-hidden="true" cdu-on@click="this.methods.handleClick">
    <use cdu-bind:href="'#icon-' + this.prop.icon"></use>
</svg>`,p=`:host {
    display: inline-block;
    vertical-align: middle;
    width: 1em;
    height: 1em;
    line-height: 1em;
    overflow: hidden;
}
.icon {
    display: inline-block;
    width: 1em;
    height: 1em;
    fill: currentColor;
}`;const m={props:{icon:{type:"string",description:"图标名称"}},events:{click:{name:"cdu-icon-click",description:"点击事件",eventArgs:"CustomEvent"}}},n="cdu-icon";class u extends d{constructor(){super(...arguments),this.name=n,this.themeStyleSheet=h,this.commonStyleSheet=l,this.styleSheet=p,this.html=r,this.propTypes=m,this.beforeMount=()=>{a()},this.updateIcon=()=>{var t;let e=window,i;for(;e&&(i=e.document.querySelector(`#icon-${this.prop.icon}`),!(i||e===window.top));)e=e.parent;if(i){const o=document.createElement("svg");o.style.cssText="position:absolute;width:0px;height:0px;overflow:hidden;",o.appendChild(i.cloneNode(!0)),(t=this.shadowRoot)===null||t===void 0||t.appendChild(o)}s(window,"load",this.updateIcon)},this.mounted=()=>{document.readyState!=="complete"?c(window,"load",this.updateIcon):setTimeout(()=>{this.updateIcon()},500)},this.updated=()=>{setTimeout(()=>{this.updateIcon()})},this.methods={handleClick:t=>{var e;this.emitEvent(t,(e=this.propTypes.events)===null||e===void 0?void 0:e.click.name)}}}}window.customElements.define(n,u);export{m as c};
