import{R as m}from"./index-DH5ua8nC.js";import{c as a}from"./index-BAMUwV84.js";import{g as u,p as c}from"./index-HqsbNbUy.js";import{w as p}from"./decorator-D4QhQYJ-.js";import{E as l}from"./Events-DxCy3JHV.js";const{argTypes:d,defaultArgs:i}=c(a),n=a.events??{},E=Object.values(n).map(t=>t.name),f=()=>m.createElement(l,{events:n}),g={title:"组件列表/Button 按钮",parameters:{actions:{handles:E}},decorators:[p],render:t=>u(t,"cdu-button"),argTypes:d,args:i,excludeStories:["EventsTable"]},e={name:"组件体验",args:{size:"medium","slots:default":"我是按钮"}};var s,o,r;e.parameters={...e.parameters,docs:{...(s=e.parameters)==null?void 0:s.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    size: 'medium',
    'slots:default': \`我是按钮\`
  }
}`,...(r=(o=e.parameters)==null?void 0:o.docs)==null?void 0:r.source}}};const b=["EventsTable","Example"],A=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:f,Example:e,__namedExportsOrder:b,default:g},Symbol.toStringTag,{value:"Module"}));export{A as D,f as E,e as a};
