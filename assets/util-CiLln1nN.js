let o=!1;try{o=!0}catch{}const c=(t,l=500)=>{let e=null;return(...n)=>{e&&clearTimeout(e),e=setTimeout(()=>{t(...n),e=null},l)}};export{c as n};
