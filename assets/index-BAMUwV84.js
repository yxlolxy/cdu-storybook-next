import{p as r,c as s,i}from"./Events-DxCy3JHV.js";var n=`<button 
    cdu-bind:class="this.prop.type + ' ' + this.prop.size" 
    cdu-on@click="this.methods.handleClick"
    cdu-on@dblclick="this.methods.handleDblClick">
    <slot></slot>
</button>`,c=`:host {
    display: inline-block;
    vertical-align: middle;
}
button {
    width: inherit;
    cursor: pointer;
    border-radius: 0.2em;
    border: none;
    padding: 0.5em 1em;
}
button:hover {
    opacity: 0.8;
}
.default:active, .border:active {
    transform: scale(0.98);
    box-shadow: 2px 2px 22px 1px var(--cdu-shadow);
}
.default {
    background-color: var(--cdu-btn-color);
    color: var(--cdu-theme);
}
.text {
    background-color: var(--cdu-transparent);
    color:var(--cdu-btn-color);
}
.border{
    border: solid 1px var(--cdu-btn-color);
    color: var(--cdu-btn-color);
    background-color: var(--cdu-transparent);
}
.small {
    font-size: 0.8em;
}
.medium {
    font-size: 1em;
}
.large {
    font-size: 1.2em;
}`;const d={props:{size:{type:["small","medium","large"],default:"medium",description:"按钮大小控制"},type:{type:["default","text","border"],default:"default",description:"按钮类型"}},slots:["default"],events:{click:{name:"cdu-button-click",description:"点击事件",eventArgs:"CustomEvent"},dbClick:{name:"cdu-button-db-click",description:"双击事件",eventArgs:"CustomEvent"}}},o="cdu-button";class l extends r{constructor(){super(...arguments),this.name=o,this.commonStyleSheet=s,this.styleSheet=c,this.html=n,this.propTypes=d,this.beforeMount=()=>{i()},this.methods={handleClick:e=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.click.name)},handleDblClick:e=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.dbClick.name)}}}}window.customElements.define(o,l);export{d as c};
