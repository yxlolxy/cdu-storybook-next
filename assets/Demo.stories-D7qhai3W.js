import{R as l}from"./index-DH5ua8nC.js";import{E as i}from"./Events-DxCy3JHV.js";import{c as m}from"./index-DSYqMolv.js";import{g as p}from"./index-HqsbNbUy.js";const d=m.events??{},u=()=>l.createElement(i,{events:d}),E={title:"组件列表/Icon 图标",render:c=>{const t=document.createElement("div"),o=document.createElement("p");o.style.fontStyle="italic",o.textContent="iconfont图标库，请在宿主项目中引入iconfont.js后使用！！！",t.appendChild(o);const n=p(c,"cdu-icon");return n.style.fontSize="25px",t.appendChild(n),t},argTypes:{icon:{control:"select",options:["upload","download","alarm","config","chat"]},"cdu-style":{control:"text",description:"css样式注入"}},excludeStories:["EventsTable"]},e={name:"组件体验",args:{icon:"upload"}};var r,s,a;e.parameters={...e.parameters,docs:{...(r=e.parameters)==null?void 0:r.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    icon: 'upload'
  }
}`,...(a=(s=e.parameters)==null?void 0:s.docs)==null?void 0:a.source}}};const f=["EventsTable","Example"],_=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:u,Example:e,__namedExportsOrder:f,default:E},Symbol.toStringTag,{value:"Module"}));export{_ as D,u as E,e as a};
