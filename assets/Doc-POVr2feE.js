import{j as t}from"./jsx-runtime-DQ32znRX.js";import{M as m,C as p}from"./index-UEcy47Qd.js";import{D as a,E as n,a as c}from"./Demo.stories-k3muN80l.js";import{useMDXComponents as s}from"./index-mlbUBBkt.js";import"./index-DH5ua8nC.js";import"./_commonjsHelpers-Cpj98o6Y.js";import"./iframe-BH84BkzF.js";import"../sb-preview/runtime.js";import"./index-BNKdAXne.js";import"./index-DrFu-skq.js";import"./index-BAMUwV84.js";import"./Events-DxCy3JHV.js";import"./index-HqsbNbUy.js";import"./decorator-D4QhQYJ-.js";import"./preview-errors-Dg5lG0lC.js";function r(o){const e=Object.assign({h3:"h3",hr:"hr"},s(),o.components);return a||i("Demo",!1),n||i("Demo.EventsTable",!0),t.jsxs(t.Fragment,{children:[t.jsx(m,{title:"组件列表/Button 按钮"}),`
`,t.jsx(e.h3,{id:"入参说明",children:"入参说明"}),`
`,t.jsx(p,{of:c}),`
`,t.jsx(e.hr,{}),`
`,t.jsx(e.h3,{id:"事件说明",children:"事件说明"}),`
`,t.jsx(n,{})]})}function y(o={}){const{wrapper:e}=Object.assign({},s(),o.components);return e?t.jsx(e,Object.assign({},o,{children:t.jsx(r,o)})):r(o)}function i(o,e){throw new Error("Expected "+(e?"component":"object")+" `"+o+"` to be defined: you likely forgot to import, pass, or provide it.")}export{y as default};
