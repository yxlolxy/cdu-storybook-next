import{R as d}from"./index-DH5ua8nC.js";import{p as c,t as h,c as b,i as m,E as v}from"./Events-DxCy3JHV.js";import{n as f}from"./util-CiLln1nN.js";import{g as w,p as g}from"./index-HqsbNbUy.js";import{w as x}from"./decorator-D4QhQYJ-.js";var y=`<div cdu-bind:class="
    'input ' + 
    this.prop.type + 
    ' ' + (this.prop.showError ? 'error' : '')
    ">
    <div class="label" cdu-if="!!this.prop.inputLabel" cdu-bind:style="
           'width:' + (this.prop.labelWidth || '') + ';' + 
           'text-align:' + (this.prop.labelTextAlign ?? '') + ';'
           ">
        {{this.prop.inputLabel}}
    </div>
    <div class="input-block">
        <input cdu-bind:value="this.prop.value" cdu-bind:type="this.prop.inputType"
            cdu-bind:placeholder="this.prop.placeholder" cdu-on@change="this.methods.handleChange"
            cdu-on@input="this.methods.handleInput" cdu-on@focus="this.methods.handleFocus"
            cdu-on@blur="this.methods.handleBlur" />
        <div class="suffix" cdu-if="this.prop.showSuffix">
            <slot name="suffix"></slot>
        </div>
        <div cdu-bind:class="'dropdown ' + (this.prop.showDropDown && this.state.showDropDown ? 'show' : 'hide')">
            <slot name="dropdown"></slot>
        </div>
    </div>
</div>`,E=`:host {
    display: inline-block;
    vertical-align: middle;
    width: inherit;
    height: inherit;
}
.input {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    width: 100%;
    height: 100%;
}

.lable, .input-block {
    height: 100%;
}

.label, input {
    font-size: 1em;
}

.block .label, .block-line-border .label {
    width: 100%!important;
}

.label {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    color: var(--cdu-color);
    font-weight: 600;
}

input {
    color: var(--cdu-color);
}

.input-block {
    position: relative;
}

.inline .input-block, .inline-line-border .input-block {
    flex: 1;
    height: 100%;
}
.block .input-block, .block-line-border .input-block {
    width: 100%;
}

input { 
    width: 100%;
    height: 100%;
}

.label, input {
    padding: 0.5em;
}

.inline input, .block input {
    border: solid 1px var(--cdu-border);
}

.inline-line-border input, .block-line-border input{
    border: none;
    border-bottom: solid 2px var(--cdu-border);
}

.suffix {
    position: absolute;
    right: 0.5em;
    top: 50%;
    transform: translateY(-50%);
}

.dropdown {
    position: absolute;
    left:0;
    bottom: 0;
    transform: translateY(100%);
    width: 100%;
    overflow-x: hidden;
}

input:focus-visible {
    outline: none;
}

.inline input:hover,.block input:hover {
    border-color: var(--cdu-btn-color);
}

.inline input:focus,.block input:focus {
    border-color: var(--cdu-transparent);;
    box-shadow: 0 0 0 2px var(--cdu-btn-shadow);
}

.inline-line-border input:focus,.block-line-border input:focus {
    border-color: var(--cdu-btn-color);
}

.error input {
    border-color: var(--cdu-error-color)!important;
    box-shadow: none!important;
}

.dropdown.show {
    display: block;
}

.dropdown.hide {
    display: none;
}`;const s={props:{type:{type:["inline","block","inline-line-border","block-line-border"],default:"inline",description:"输入框类型"},showError:{type:"boolean",default:!1,description:"输入框是否有误"},inputLabel:{type:"string",description:"输入框标签"},value:{type:"string",description:"输入框值"},inputType:{type:["text","password"],default:"text",description:"input的文本type，仅支持text和password"},labelWidth:{type:"string",default:"4em",description:"输入框标签长度，type=inline时生效"},labelTextAlign:{type:["left","center","right"],default:"left",description:"输入框标签文字对齐方式"},placeholder:{type:"string",default:"",description:"输入框提示"},showSuffix:{type:"boolean",default:!1,description:"是否显示后缀"},showDropDown:{type:"boolean",default:!1,description:"是否显示下拉"}},slots:["suffix","dropdown"],events:{input:{name:"cdu-input-input",description:"输入改变事件",eventArgs:"CustomEvent"},change:{name:"cdu-input-change",description:"输入改变事件",eventArgs:"CustomEvent"},focus:{name:"cdu-input-focus",description:"输入聚焦事件",eventArgs:"CustomEvent"},blur:{name:"cdu-input-blur",description:"输入失焦事件",eventArgs:"CustomEvent"}}},a="cdu-input";class k extends c{constructor(){super(...arguments),this.name=a,this.themeStyleSheet=h,this.commonStyleSheet=b,this.styleSheet=E,this.html=y,this.propTypes=s,this.state={showDropDown:!1},this.beforeMount=()=>{m()},this.debounceInput=f((e,t)=>{var i;this.emitEvent(e,(i=this.propTypes.events)===null||i===void 0?void 0:i.input.name,{value:t})}),this.methods={handleInput:e=>{this.debounceInput(e,e.target.value)},handleChange:e=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.change.name,{value:e.target.value})},handleFocus:e=>{this.setState({showDropDown:!0}),setTimeout(()=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.focus.name,{value:e.target.value})})},handleBlur:e=>{this.setState({showDropDown:!1}),setTimeout(()=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.blur.name,{value:e.target.value})})}}}}window.customElements.define(a,k);const{argTypes:S,defaultArgs:T}=g(s),u=s.events??{},D=Object.values(u).map(n=>n.name),C=()=>d.createElement(v,{events:u}),_={title:"组件列表/Input 输入框",parameters:{actions:{handles:D}},decorators:[x],render:n=>w(n,"cdu-input"),argTypes:S,args:T,excludeStories:["EventsTable"]},o={name:"组件体验",args:{type:"inline",inputLabel:"标签",placeholder:"请输入",value:"xxxx",showSuffix:!0,showDropDown:!0,"slots:suffix":"♥","slots:dropdown":"我是下拉内容"}};var r,l,p;o.parameters={...o.parameters,docs:{...(r=o.parameters)==null?void 0:r.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    type: 'inline',
    inputLabel: '标签',
    placeholder: '请输入',
    value: 'xxxx',
    showSuffix: true,
    showDropDown: true,
    'slots:suffix': '♥',
    'slots:dropdown': '我是下拉内容'
  }
}`,...(p=(l=o.parameters)==null?void 0:l.docs)==null?void 0:p.source}}};const A=["EventsTable","Example"],j=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:C,Example:o,__namedExportsOrder:A,default:_},Symbol.toStringTag,{value:"Module"}));export{j as D,C as E,o as a};
