import{R as c}from"./index-DH5ua8nC.js";import{p as h,t as u,c as m,i as v,E as g}from"./Events-DxCy3JHV.js";import{n as b}from"./util-CiLln1nN.js";import{g as x,p as y}from"./index-HqsbNbUy.js";import{w as f}from"./decorator-D4QhQYJ-.js";var w=`<div class="textarea">
    <div class="label" cdu-if="!!this.prop.inputLabel">
        {{this.prop.inputLabel}}
    </div>
    <div class="textarea-block">
        <textarea cdu-bind:value="this.prop.value" cdu-bind:placeholder="this.prop.placeholder"
            cdu-bind:maxLength="this.prop.maxLength" cdu-bind:rows="this.prop.rows"
            cdu-on@input="this.methods.handleInput" cdu-on@change="this.methods.handleChange" />
    </div>
</div>`,E=`:host {
    display: inline-block;
    vertical-align: middle;
    width: inherit;
    height: inherit;
}
.textarea {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    width: 100%;
    height: 100%;
}

.label, .textarea-block {
    width: 100%;
    height: 100%;
}

.label, textarea {
    font-size: 1em;
    padding: 0.5em;
}

.label {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    color: var(--cdu-color);
    font-weight: 600;
}

textarea {
    width: 100%;
    color: var(--cdu-color);
    border: solid 1px var(--cdu-border);
}

textarea:focus-visible {
    outline: none;
}`;const n={props:{inputLabel:{type:"string",description:"输入框标签"},value:{type:"string",description:"输入框值"},labelTextAlign:{type:["left","center","right"],default:"left",description:"输入框标签文字对齐方式"},placeholder:{type:"string",default:"",description:"输入框提示"},maxLength:{type:"number",description:"用户输入的最大字符串长度"},rows:{type:"number",default:4,description:"显示控件的指定的可见文本行数"}},events:{input:{name:"cdu-textarea-input",description:"输入改变事件",eventArgs:"CustomEvent"},change:{name:"cdu-textarea-change",description:"输入改变事件",eventArgs:"CustomEvent"}}},p="cdu-textarea";class T extends h{constructor(){super(...arguments),this.name=p,this.themeStyleSheet=u,this.commonStyleSheet=m,this.styleSheet=E,this.html=w,this.propTypes=n,this.state={showDropDown:!1},this.beforeMount=()=>{v()},this.debounceInput=b((e,t)=>{var r;this.emitEvent(e,(r=this.propTypes.events)===null||r===void 0?void 0:r.input.name,{value:t})}),this.methods={handleInput:e=>{this.debounceInput(e,e.target.value)},handleChange:e=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.change.name,{value:e.target.value})}}}}window.customElements.define(p,T);const{argTypes:S,defaultArgs:_}=y(n),d=n.events??{},C=Object.values(d).map(s=>s.name),L=()=>c.createElement(g,{events:d}),A={title:"组件列表/Textarea 富文本框",parameters:{actions:{handles:C}},decorators:[f],render:s=>{const e=x(s,"cdu-textarea");return e.style.width="500px",e},argTypes:S,args:_,excludeStories:["EventsTable"]},a={name:"组件体验",args:{inputLabel:"标签",placeholder:"请输入",value:"yyyy"}};var o,i,l;a.parameters={...a.parameters,docs:{...(o=a.parameters)==null?void 0:o.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    inputLabel: '标签',
    placeholder: '请输入',
    value: 'yyyy'
  }
}`,...(l=(i=a.parameters)==null?void 0:i.docs)==null?void 0:l.source}}};const D=["EventsTable","Example"],R=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:L,Example:a,__namedExportsOrder:D,default:A},Symbol.toStringTag,{value:"Module"}));export{R as D,L as E,a};
