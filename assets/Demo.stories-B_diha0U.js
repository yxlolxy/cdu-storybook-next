import{R as f}from"./index-DH5ua8nC.js";import{t as p,n as d}from"./dom-D5Pft_32.js";import{n as b}from"./util-CiLln1nN.js";import{p as y,t as g,c as E,i as w,n as x,E as C}from"./Events-DxCy3JHV.js";import{a as k,b as T}from"./helper-CUqIugP9.js";import{g as S,p as P}from"./index-HqsbNbUy.js";import{w as _}from"./decorator-D4QhQYJ-.js";import"./index-BAMUwV84.js";import"./index-DSYqMolv.js";var D=`<div cdu-bind:class="'popup' + (this.prop.direction ? ' ' + this.prop.direction: '')" cdu-if="this.prop.visible">
    <div class="header" cdu-if="this.prop.showHeader">
        <span class="title">{{this.prop.headTitle}}</span>
        <span class="close" cdu-if="this.prop.showClose" cdu-on@click="this.methods.handleClose"></span>
    </div>
    <div class="content">
        <slot></slot>
    </div>
    <div class="footer" cdu-if="this.prop.showFooter">
        <slot name="footer"></slot>
    </div>
</div>`,F=`:host {
    display: block;
}
.popup {
    color: var(--cdu-color);
    background-color: var(--cdu-theme);
    box-shadow: 2px 2px 22px 1px var(--cdu-shadow);
    border-radius: 5px;
}
.header {
    display: flex;
    justify-content: space-between;
    align-items: center;
}
.popup .header .title {
    font-size: 1.2em;
    font-weight: 600;
    color: var(--cdu-color);  
}
.close {
    position: relative;
    display: inline-block;
    width: 1em;
    height: 1em;
    cursor: pointer;
    border-radius: 50%;
}
.close:hover {
    opacity: 0.8;
}
.close:active {
    transform: scale(0.98);
    box-shadow: 2px 2px 22px 1px var(--cdu-shadow);
}
.close::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0.5em;
    width: 1px;
    height: 1em;
    background-color: var(--cdu-color);
    transform: rotate(45deg);
}
.close::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0.5em;
    width: 1px;
    height: 1em;
    background-color: var(--cdu-color);
    transform: rotate(-45deg);
}
.header,.content, .footer {
    padding: 0.8em;
}

.footer {
    display: flex;
    align-items: center;
    justify-content: flex-end;
}`;const a={props:{visible:{type:"boolean",default:!1,description:"是否显示弹窗"},showFooter:{type:"boolean",default:!0,description:"是否显示弹窗底部"},showHeader:{type:"boolean",default:!0,description:"是否显示弹窗顶部"},showClose:{type:"boolean",default:!0,description:"是否显示关闭按钮"},headTitle:{type:"string",description:"弹窗标题"},direction:{type:["lt","lb","rt","rb"],description:"弹窗应该出现的位置，lt左上，lb左下，rt右上，rb右下"},trigger:{type:["click","hover"],default:"click",description:"失焦隐藏触发方式"}},slots:["default","footer"],events:{close:{name:"cdu-popup-close",description:"弹窗关闭事件",eventArgs:"CustomEvent"}}},m="cdu-popup";class j extends y{constructor(){super(...arguments),this.name=m,this.themeStyleSheet=g,this.commonStyleSheet=E,this.styleSheet=F,this.html=D,this.propTypes=a,this.parentEl=null,this.containerEl=k(),this.beforeMount=()=>{w(),document.body.appendChild(this.containerEl)},this.updatePositon=()=>{if(!this.prop.visible)return;const e=T(this.parentEl,this.prop.direction);this.containerEl&&Object.keys(e).forEach(t=>{this.containerEl.style[t]=e[t]})},this.updatePositonDebounce=b(this.updatePositon,200),this.handleWindowClose=e=>{var t,i,n;if(!this.prop.visible)return;const{target:l}=e;!(!((t=this.parentEl)===null||t===void 0)&&t.contains(l))&&!(!((i=this.realShadowRoot)===null||i===void 0)&&i.contains(l))&&!(!((n=this.containerEl)===null||n===void 0)&&n.contains(l))&&this.methods.handleClose(e)},this.watchVisible=()=>{const{visible:e}=this.prop,t=this.containerEl;e?(this.updatePositon(),t&&(x(t),t.style.display="block")):t&&(t.style.display="none")},this.mounted=()=>{this.parentEl=this.parentElement,p(window,"resize",this.updatePositonDebounce),p(window,this.prop.trigger==="click"?"click":"mouseover",this.handleWindowClose),this.patchMoved(),this.containerEl.appendChild(this),this.watchVisible()},this.updated=()=>{this.watchVisible()},this.beforeDestroy=()=>{var e;(e=this.containerEl.parentNode)===null||e===void 0||e.removeChild(this.containerEl),d(window,"resize",this.updatePositonDebounce),d(window,this.prop.trigger==="click"?"click":"mouseover",this.handleWindowClose),this.unpatchMoved()},this.methods={handleClose:e=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.close.name)}}}}window.customElements.define(m,j);const{argTypes:A,defaultArgs:O}=P(a),v=a.events??{},z=Object.values(v).map(s=>s.name),L=()=>f.createElement(C,{events:v});let o;const M={title:"组件列表/Popup 弹窗",parameters:{actions:{handles:z}},decorators:[_],render:s=>{document.body.style.height="100vh";const e=document.createElement("div");e.style.display="inline-block",e.style.backgroundColor="#1890ff",e.style.color="#fff",e.style.padding="10px 30px",e.style.marginLeft="250px",e.style.marginTop="250px",e.style.cursor="pointer";const t=document.createTextNode("显隐切换");e.appendChild(t),o&&o.parentNode.remove(),o=S(s,"cdu-popup");const i=document.createElement("button");i.textContent="点击一下",e.appendChild(o);const{trigger:n}=s;return n==="click"?e.addEventListener("click",()=>{o.visible=!o.visible}):e.addEventListener("mouseenter",()=>{o.visible=!0}),o.addEventListener("cdu-popup-close",()=>{o.visible=!1}),e},argTypes:A,args:O,excludeStories:["EventsTable"]},r={name:"组件体验",args:{headTitle:"标题","slots:footer":"我是弹窗底部",visible:!1,direction:"rb"}};var c,h,u;r.parameters={...r.parameters,docs:{...(c=r.parameters)==null?void 0:c.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    headTitle: '标题',
    // 'slots:default': '<div style="width: 250px">我是内容<cdu-icon icon="upload"></cdu-icon></div>',
    'slots:footer': '我是弹窗底部',
    visible: false,
    direction: 'rb'
  }
}`,...(u=(h=r.parameters)==null?void 0:h.docs)==null?void 0:u.source}}};const N=["EventsTable","Example"],J=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:L,Example:r,__namedExportsOrder:N,default:M},Symbol.toStringTag,{value:"Module"}));export{J as D,L as E,r as a};
