import{R as p}from"./index-DH5ua8nC.js";import{p as h,t as u,c as m,i as f,n as v,E as b}from"./Events-DxCy3JHV.js";import{g}from"./helper-CUqIugP9.js";import{t as y,n as E}from"./dom-D5Pft_32.js";import"./index-BAMUwV84.js";import{g as x,p as C}from"./index-HqsbNbUy.js";import{w}from"./decorator-D4QhQYJ-.js";var k=`<div class="dialog" cdu-if="this.prop.visible">
    <div class="header" cdu-if="this.prop.showHeader">
        <span class="title">{{this.prop.headTitle}}</span>
        <span class="close" cdu-if="this.prop.showClose" cdu-on@click="this.methods.handleClose"></span>
    </div>
    <div class="content">
        <slot></slot>
    </div>
    <div class="footer" cdu-if="this.prop.showFooter">
        <slot name="footer"></slot>
    </div>
</div>`,T=`:host {
    display: block;
    position: absolute;
    top: 20%;
    left: 50%;
    transform: translateX(-50%);
}
.dialog {
    display: block;
    color: var(--cdu-color);
    background-color: var(--cdu-theme);
    box-shadow: 2px 2px 22px 1px var(--cdu-shadow);
    border-radius: 5px;
}
.header {
    display: flex;
    justify-content: space-between;
    align-items: center;
}
.dialog .header .title {
    font-size: 1.2em;
    font-weight: 600;
    color: var(--cdu-color);  
}
.close {
    position: relative;
    display: inline-block;
    width: 1em;
    height: 1em;
    cursor: pointer;
    border-radius: 50%;
}
.close:hover {
    opacity: 0.8;
}
.close:active {
    transform: scale(0.98);
    box-shadow: 2px 2px 22px 1px var(--cdu-shadow);
}
.close::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0.5em;
    width: 1px;
    height: 1em;
    background-color: var(--cdu-color);
    transform: rotate(45deg);
}
.close::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0.5em;
    width: 1px;
    height: 1em;
    background-color: var(--cdu-color);
    transform: rotate(-45deg);
}
.header,.content, .footer {
    padding: 0.8em;
}

.footer {
    display: flex;
    align-items: center;
    justify-content: flex-end;
}`;const a={props:{visible:{type:"boolean",default:!1,description:"是否显示弹窗"},showFooter:{type:"boolean",default:!0,description:"是否显示弹窗底部"},showHeader:{type:"boolean",default:!0,description:"是否显示弹窗顶部"},showClose:{type:"boolean",default:!0,description:"是否显示关闭按钮"},showMask:{type:"boolean",default:!0,description:"是否显示遮罩"},maskClosable:{type:"boolean",default:!0,description:"点击遮罩能否关闭"},headTitle:{type:"string",description:"弹窗标题"}},events:{close:{name:"cdu-dialog-close",description:"弹窗关闭事件",eventArgs:"CustomEvent"}}},d="cdu-dialog";class S extends h{constructor(){super(...arguments),this.name=d,this.themeStyleSheet=u,this.commonStyleSheet=m,this.styleSheet=T,this.html=k,this.propTypes=a,this.containerEl=g(),this.beforeMount=()=>{f(),document.body.appendChild(this.containerEl)},this.handleContainerClick=e=>{if(e.target===this.containerEl){const{maskClosable:t}=this.prop;t&&this.methods.handleClose(e)}},this.handleContainer=()=>{if(this.containerEl){const{showMask:e,visible:t}=this.prop;this.containerEl.style.backgroundColor=e?"var(--cdu-mask)":"",this.containerEl.style.display=t?"block":"none",t&&v(this.containerEl)}},this.mounted=()=>{y(this.containerEl,"click",this.handleContainerClick),this.handleContainer(),this.patchMoved(),this.containerEl.appendChild(this)},this.updated=()=>{this.handleContainer()},this.beforeDestroy=()=>{var e;(e=this.containerEl.parentNode)===null||e===void 0||e.removeChild(this.containerEl),E(this.containerEl,"click",this.handleContainerClick),this.unpatchMoved()},this.methods={handleClose:e=>{var t;this.emitEvent(e,(t=this.propTypes.events)===null||t===void 0?void 0:t.close.name)}}}}window.customElements.define(d,S);const{argTypes:_,defaultArgs:D}=C(a),c=a.events??{},A=Object.values(c).map(i=>i.name),M=()=>p.createElement(b,{events:c});let o;const j={title:"组件列表/Dialog 弹窗",parameters:{actions:{handles:A}},decorators:[w],render:i=>{document.body.style.height="100vh";const e=document.createElement("div");e.style.display="inline-block",e.style.backgroundColor="#1890ff",e.style.color="#fff",e.style.padding="10px 30px",e.style.marginLeft="250px",e.style.marginTop="250px",e.style.cursor="pointer";const t=document.createTextNode("显隐切换");return e.appendChild(t),o&&o.parentNode.remove(),o=x(i,"cdu-dialog"),e.appendChild(o),e.addEventListener("click",()=>{o.visible=!0}),o.addEventListener("cdu-dialog-close",()=>{o.visible=!1}),e},argTypes:_,args:D,excludeStories:["EventsTable"]},s={name:"组件体验",args:{headTitle:"标题","slots:default":'<div style="width: 250px">我是内容</div>',"slots:footer":"<cdu-button>按钮</cdu-button>",visible:!1}};var n,l,r;s.parameters={...s.parameters,docs:{...(n=s.parameters)==null?void 0:n.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    headTitle: '标题',
    'slots:default': '<div style="width: 250px">我是内容</div>',
    'slots:footer': '<cdu-button>按钮</cdu-button>',
    visible: false
  }
}`,...(r=(l=s.parameters)==null?void 0:l.docs)==null?void 0:r.source}}};const F=["EventsTable","Example"],P=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:M,Example:s,__namedExportsOrder:F,default:j},Symbol.toStringTag,{value:"Module"}));export{P as D,M as E,s as a};
