import{j as n}from"./jsx-runtime-DQ32znRX.js";import{M as t}from"./index-UEcy47Qd.js";import"./Demo.stories-k3muN80l.js";import{useMDXComponents as s}from"./index-mlbUBBkt.js";import"./index-DH5ua8nC.js";import"./_commonjsHelpers-Cpj98o6Y.js";import"./iframe-BH84BkzF.js";import"../sb-preview/runtime.js";import"./index-BNKdAXne.js";import"./index-DrFu-skq.js";import"./index-BAMUwV84.js";import"./Events-DxCy3JHV.js";import"./index-HqsbNbUy.js";import"./decorator-D4QhQYJ-.js";import"./preview-errors-Dg5lG0lC.js";function i(r){const e=Object.assign({h2:"h2",ul:"ul",li:"li",p:"p",a:"a",hr:"hr",h3:"h3",h4:"h4",code:"code",blockquote:"blockquote",strong:"strong",pre:"pre",img:"img"},s(),r.components);return n.jsxs(n.Fragment,{children:[n.jsx(t,{title:"使用指南"}),`
`,n.jsx(e.h2,{id:"目录",children:"目录"}),`
`,n.jsxs(e.ul,{children:[`
`,n.jsxs(e.li,{children:[`
`,n.jsx(e.p,{children:n.jsx(e.a,{href:"#%E5%BC%80%E5%A7%8B%E4%BD%BF%E7%94%A8",children:"开始使用"})}),`
`,n.jsxs(e.ul,{children:[`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E5%91%BD%E4%BB%A4%E8%A1%8C%E5%AE%89%E8%A3%85",children:"命令行安装"})}),`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E5%AF%BC%E5%85%A5%E7%BB%84%E4%BB%B6",children:"导入组件"})}),`
`]}),`
`]}),`
`,n.jsxs(e.li,{children:[`
`,n.jsx(e.p,{children:n.jsx(e.a,{href:"#%E7%94%A8%E6%B3%95%E8%AF%B4%E6%98%8E",children:"用法说明"})}),`
`,n.jsxs(e.ul,{children:[`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E5%A4%9A%E4%B8%BB%E9%A2%98%E5%8C%96",children:"多主题化"})}),`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E4%B8%8Evue2%E4%B8%80%E8%B5%B7%E4%BD%BF%E7%94%A8",children:"与vue2一起使用"})}),`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E4%B8%8Evue3%E4%B8%80%E8%B5%B7%E4%BD%BF%E7%94%A8",children:"与vue3一起使用"})}),`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E4%B8%8Eangular%E4%B8%80%E8%B5%B7%E4%BD%BF%E7%94%A8",children:"与angular一起使用"})}),`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E4%B8%8Ereact%E4%B8%80%E8%B5%B7%E4%BD%BF%E7%94%A8",children:"与react一起使用"})}),`
`,n.jsx(e.li,{children:n.jsx(e.a,{href:"#%E4%B8%8Esvelte%E4%B8%80%E8%B5%B7%E4%BD%BF%E7%94%A8",children:"与svelte一起使用"})}),`
`]}),`
`]}),`
`]}),`
`,n.jsx(e.hr,{}),`
`,n.jsx(e.h3,{id:"安装指南",children:"安装指南"}),`
`,n.jsx(e.h4,{id:"命令行安装",children:"命令行安装"}),`
`,n.jsxs(e.p,{children:[n.jsx(e.code,{children:"npm i code-design-ui"})," 或 ",n.jsx(e.code,{children:"yarn add code-design-ui"}),"'"]}),`
`,n.jsx(e.h4,{id:"导入组件",children:"导入组件"}),`
`,n.jsxs(e.ul,{children:[`
`,n.jsx(e.li,{children:"全部导入"}),`
`]}),`
`,n.jsx(e.p,{children:n.jsx(e.code,{children:"import * from 'code-design-ui'"})}),`
`,n.jsxs(e.ul,{children:[`
`,n.jsx(e.li,{children:"单独导入 (推荐)"}),`
`]}),`
`,n.jsx(e.p,{children:n.jsx(e.code,{children:"import CduButton from 'code-design-ui/dist/components/cdu-button'"})}),`
`,n.jsxs(e.blockquote,{children:[`
`,n.jsx(e.p,{children:"提示: 部分导入会导入组件javascript和样式以及全局主题样式，其他组件javasript和样式均不会被导入！"}),`
`]}),`
`,n.jsx(e.h3,{id:"用法说明",children:"用法说明"}),`
`,n.jsx(e.h4,{id:"多主题化",children:"多主题化"}),`
`,n.jsx(e.p,{children:"待续"}),`
`,n.jsx(e.h4,{id:"与vue2一起使用",children:"与vue2一起使用"}),`
`,n.jsx(e.p,{children:n.jsxs(e.strong,{children:["Vue2需要在入口处增加",n.jsx(e.code,{children:"Vue.config.ignoredElements = [/^cdu-/]"}),"，可以避免以下警告信弹出："]})}),`
`,n.jsx(e.pre,{children:n.jsx(e.code,{className:"language-javascript",children:`vue2.7.6..js:5129 [Vue warn]: Unknown custom element:\r
<cdu-layout> - did you register the component correctly?\r
For recursive components, make sure to provide the "name" option 
`})}),`
`,n.jsxs(e.p,{children:[n.jsx(e.strong,{children:"使用案例"}),"："]}),`
`,n.jsx(e.pre,{children:n.jsx(e.code,{className:"language-javascript",children:`<template>\r
  <cdu-button @cdu-button-click="handleClick" :size.prop="size">\r
        Button\r
  </cdu-button>\r
</template>\r
<script>\r
  import CduButton from 'code-design-ui/dist/components/cdu-button'\r
  Vue.config.ignoredElements = [/^cdu-/] // 重要！！！必须设置\r
  export default {\r
    data() {\r
        return {\r
            size: 'large',\r
        }\r
    },\r
    methods: {\r
        handleClick(e) {\r
            if (this.size == 'large') {\r
                this.size = 'small'\r
            } else if (this.size === 'medium') {\r
                this.size = 'large'\r
            } else {\r
                this.size = 'medium'\r
            }\r
        },\r
    },\r
  }\r
<\/script>\r

`})}),`
`,n.jsxs(e.blockquote,{children:[`
`,n.jsx(e.p,{children:"提示: 入参如果是string/number/boolean可以不用.prop修饰符使用，__在vue2.x版本中，使用attribute偶尔会导致响应式无法生效！！！__建议始终采用.prop修饰符！"}),`
`]}),`
`,n.jsx(e.p,{children:n.jsx(e.img,{src:"https://free-cdn.netlify.app/code-design-ui/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E-vue2.gif",alt:"vue2图示"})}),`
`,n.jsx(e.hr,{}),`
`,n.jsx(e.h4,{id:"与vue3一起使用",children:"与vue3一起使用"}),`
`,n.jsx(e.p,{children:n.jsxs(e.strong,{children:["同样为了避免警告信息，需要在入口处增加",n.jsx(e.code,{children:"app.config.compilerOptions.isCustomElement = (tag) => tag.startsWith('cdu-')"})]})}),`
`,n.jsx(e.p,{children:n.jsx(e.strong,{children:"使用案例："})}),`
`,n.jsx(e.pre,{children:n.jsx(e.code,{className:"language-javascript",children:`// main.js\r
app.config.compilerOptions.isCustomElement = (tag) => tag.startsWith('cdu-') // 重要！！！必须设置\r
<template>\r
  <cdu-button slot="left" @cdu-button-click="handleClick" :size.prop="size">\r
        Button\r
  </cdu-button>\r
</template>\r
<script setup>\r
  import CduButton from 'code-design-ui/dist/components/cdu-button'\r
  \r
  const size = ref('small')\r
  const handleClick = () => {\r
        if (size.value == 'large') {\r
            size.value = 'small'\r
        } else if (size.value === 'medium') {\r
            size.value = 'large'\r
        } else {\r
            size.value = 'medium'\r
        }\r
    }\r
  return {\r
        size,\r
        handleClick,\r
  }\r
<\/script>
`})}),`
`,n.jsxs(e.blockquote,{children:[`
`,n.jsx(e.p,{children:"tip: 入参如果是string/number/boolean可以不用.prop修饰符使用，否则必须采用.prop修饰符才可使用，建议始终采用.prop修饰符"}),`
`]}),`
`,n.jsx(e.p,{children:n.jsx(e.img,{src:"https://free-cdn.netlify.app/code-design-ui/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E-vue3.gif",alt:"vue3图示"})}),`
`,n.jsx(e.hr,{}),`
`,n.jsx(e.h4,{id:"与angular一起使用",children:"与angular一起使用"}),`
`,n.jsx(e.pre,{children:n.jsx(e.code,{children:`//app.component.ts\r
@Component({\r
  selector: 'app-root',\r
  standalone: true,\r
  imports: [RouterOutlet],\r
  template: \`\r
  <main class="main">\r
    <cdu-button [size]="size" (cdu-button-click)="testClick()">{{btnText}}</cdu-button>\r
  </main>\r
  \`,\r
  schemas: [CUSTOM_ELEMENTS_SCHEMA]\r
})\r
export class AppComponent {\r
  title = 'test';\r
  size="large"\r
  btnText = 'large按钮'\r
  testClick() {\r
    if (this.size === 'large') {\r
        this.size = 'small'\r
    } else if (this.size === 'medium') {\r
        this.size = 'large'\r
    } else {\r
        this.size = 'medium'\r
    }\r
    this.btnText = this.size + 'button'\r
  }\r
}
`})}),`
`,n.jsx(e.p,{children:n.jsx(e.img,{src:"https://free-cdn.netlify.app/code-design-ui/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E-angular.gif",alt:"angular图示"})}),`
`,n.jsx(e.h4,{id:"与react一起使用",children:"与react一起使用"}),`
`,n.jsx(e.p,{children:n.jsx(e.strong,{children:"react对webcomponent的支持不如vue和angular，但仍可以借助useEffect或者Ref回调使用"})}),`
`,n.jsx(e.p,{children:n.jsx(e.strong,{children:"使用案例："})}),`
`,n.jsx(e.pre,{children:n.jsx(e.code,{className:"language-javascript",children:`  const [size, setSize] = useState('large')\r
  // 事件监听器方式\r
  // useEffect(() => {\r
  //     const handler = (e) => {\r
  //         if (size === 'large') {\r
  //             setSize('small')\r
  //         } else if (size === 'medium') {\r
  //             setSize('large')\r
  //         } else {\r
  //             setSize('medium')\r
  //         }\r
  //     }\r
  //     ref.current.addEventListener('cdu-button-click', handler)\r
  //     return () => {\r
  //         ref.current.removeEventListener('cdu-button-click', handler)\r
  //     }\r
  // }, [size])\r
  // ref回调方式\r
  const handler = (e) => {\r
      if (size === 'large') {\r
          setSize('small')\r
      } else if (size === 'medium') {\r
          setSize('large')\r
      } else {\r
          setSize('medium')\r
      }\r
  }\r
  return (\r
      <cdu-button ref={(el) => {\r
          if (el) {\r
              el['cdu-button-click'] = handler\r
          }\r
      }} size={size}>\r
          Button\r
      </cdu-button>\r
  )
`})}),`
`,n.jsx(e.p,{children:n.jsx(e.img,{src:"https://free-cdn.netlify.app/code-design-ui/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E-react.gif",alt:"react图示"})}),`
`,n.jsx(e.h4,{id:"与svelte一起使用",children:"与svelte一起使用"}),`
`,n.jsx(e.p,{children:n.jsxs(e.strong,{children:["svelte需要关闭ssr才能使用，关闭方式参考",n.jsx(e.a,{href:"https://kit.svelte.dev/docs/single-page-apps",target:"_blank",rel:"nofollow noopener noreferrer",children:"官网"})]})}),`
`,n.jsx(e.pre,{children:n.jsx(e.code,{children:`//+page.svelte\r
<script>\r
	import "code-design-ui/dist/components/cdu-button"\r
	let size = "medium"\r
	function handleClick() {\r
		if (size == 'large') {\r
			size = 'small'\r
		} else if (size === 'medium') {\r
			size = 'large'\r
		} else {\r
			size = 'medium'\r
		}\r
	}\r
<\/script>\r
\r
<svelte:head>\r
	<title>Svelte Button</title>\r
	<meta name="description" content="Demo" />\r
</svelte:head>\r
\r
<section>\r
	<cdu-button on:cdu-button-click={handleClick} size={size}>点我一下</cdu-button>\r
</section>
`})}),`
`,n.jsx(e.h2,{id:"angular图示",children:n.jsx(e.img,{src:"https://free-cdn.netlify.app/code-design-ui/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E-svelte.gif",alt:"angular图示"})})]})}function z(r={}){const{wrapper:e}=Object.assign({},s(),r.components);return e?n.jsx(e,Object.assign({},r,{children:n.jsx(i,r)})):i(r)}export{z as default};
