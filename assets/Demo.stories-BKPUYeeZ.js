import{R as l}from"./index-DH5ua8nC.js";import{c as a}from"./index-BfByOXgv.js";import{g as p,p as m}from"./index-HqsbNbUy.js";import{w as i}from"./decorator-D4QhQYJ-.js";import{E as d}from"./Events-DxCy3JHV.js";const{argTypes:u,defaultArgs:g}=m(a),c=a.events??{},E=Object.values(c).map(o=>o.name),f=()=>l.createElement(d,{events:c}),y={title:"组件列表/Loading 加载状态",parameters:{actions:{handles:E}},decorators:[i],render:o=>{const t=p(o,"cdu-loading");return t.style.width="250px",t.style.height="250px",t},argTypes:u,args:g,excludeStories:["EventsTable"]},e={name:"组件体验",args:{type:"loading-1","cdu-style":`
      .icon {
        font-size:25px;
        color: blue;
      }
    `}};var s,r,n;e.parameters={...e.parameters,docs:{...(s=e.parameters)==null?void 0:s.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    type: 'loading-1',
    'cdu-style': \`
      .icon {
        font-size:25px;
        color: blue;
      }
    \`
  }
}`,...(n=(r=e.parameters)==null?void 0:r.docs)==null?void 0:n.source}}};const b=["EventsTable","Example"],A=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:f,Example:e,__namedExportsOrder:b,default:y},Symbol.toStringTag,{value:"Module"}));export{A as D,f as E,e as a};
