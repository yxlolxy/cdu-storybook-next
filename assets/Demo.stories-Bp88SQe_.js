import{R as c}from"./index-DH5ua8nC.js";import"./index-BfByOXgv.js";import{p as l,t as p,c as g,i as h,E as m}from"./Events-DxCy3JHV.js";import{g as u,p as f}from"./index-HqsbNbUy.js";import{w as y}from"./decorator-D4QhQYJ-.js";var b=`<div class="img-container">
    <div class="loading" cdu-if="this.state.loading" >
        <cdu-loading 
            cdu-if="this.state.loading"
            cdu-bind:type="this.prop.loadingtype"
        ></cdu-loading>
    </div>
    <img 
        cdu-if="this.prop.srcset || this.prop.src"
        cdu-bind:src="this.prop.src"
        cdu-bind:class="this.prop.fit"
        cdu-bind:alt="this.prop.alt"
        cdu-bind:crossorigin="this.prop.crossorigin"
        cdu-bind:decoding="this.prop.decoding"
        cdu-bind:srcset="this.prop.srcset || this.prop.src"
        cdu-bind:sizes="this.prop.sizes"
        cdu-bind:loading="this.prop.loading"
        cdu-bind:referrerpolicy="this.prop.referrerpolicy"
        cdu-on@load="this.methods.handleLoaded"
        cdu-on@error="this.methods.handleError"
    ></img>
</div>
`,v=`:host {
    position: relative;
    display: inline-block;
    vertical-align: middle;
    width: 100%;
    height: 100%;
}
.img-container {
    width: inherit;
    height: inherit;
}
img {
    width: 100%;
    height: 100%;
}
.contain{
    object-fit: contain;
}
.cover{
    object-fit: cover;
}
.fill{
    object-fit: fill;
}
.scale-down{
    object-fit: scale-down;
}
.loading {
    position: absolute;
    left: 0;
    top: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
    background-color: var(--cdu-theme);
}

`;const s={props:{loadingType:{type:["loading-1","loading-2","loading-3","loading-4","loading-5","loading-6","loading-7","loading-8"],default:"loading-1",description:"加载动画类型"},fit:{type:["contain","cover","fill","scale-down"],default:"scale-down",description:"图片在父容器中放置类型"},alt:{type:"string",description:"图片备用文本描述"},crossorigin:{type:["anonymous","use-credentials"],description:"是否必须使用 CORS 完成相关图像的抓取"},decoding:{type:["sync","async","auto"],description:"为浏览器提供图像解码方式上的提示"},src:{type:"string",description:"图片src"},srcset:{type:"string",description:"高分辨率图片src"},sizes:{type:"string",description:"标识不同视口大小应该加载的资源与srcset配合使用"},loading:{type:["eager","lazy"],description:"加载图像方式",default:"lazy"},referrerpolicy:{type:["no-referrer","no-referrer-when-downgrade","origin","origin-when-cross-origin","same-origin","strict-origin","strict-origin-when-cross-origin","unsafe-url"],description:"获取图像是使用的来源地址"}}},a="cdu-image";class w extends l{constructor(){super(...arguments),this.name=a,this.themeStyleSheet=p,this.commonStyleSheet=g,this.styleSheet=v,this.html=b,this.propTypes=s,this.state={loading:!0},this.beforeMount=()=>{h()},this.methods={handleLoaded:()=>{this.setState({loading:!1})},handleError:()=>{this.setState({loading:!1})}}}}window.customElements.define(a,w);const{argTypes:S,defaultArgs:E}=f(s),d=s.events??{},T=Object.values(d).map(t=>t.name),j=()=>c.createElement(m,{events:d}),x={title:"组件列表/Image 图片",parameters:{actions:{handles:T}},decorators:[y],render:t=>{const i=document.createElement("div");return i.style.width="400px",i.style.height="300px",i.appendChild(u(t,"cdu-image")),i},argTypes:S,args:E,excludeStories:["EventsTable"]},e={name:"组件体验",args:{src:"https://free-cdn.netlify.app/bookmark/3.jpg"}};var o,r,n;e.parameters={...e.parameters,docs:{...(o=e.parameters)==null?void 0:o.docs,source:{originalSource:`{
  name: '组件体验',
  args: {
    src: 'https://free-cdn.netlify.app/bookmark/3.jpg'
  }
}`,...(n=(r=e.parameters)==null?void 0:r.docs)==null?void 0:n.source}}};const _=["EventsTable","Example"],R=Object.freeze(Object.defineProperty({__proto__:null,EventsTable:j,Example:e,__namedExportsOrder:_,default:x},Symbol.toStringTag,{value:"Module"}));export{R as D,j as E,e as a};
